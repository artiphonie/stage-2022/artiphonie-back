from datetime import date, datetime
from typing import List, Optional

from pydantic import BaseModel, Field


class EnfantBase(BaseModel):
    login : str

class EnfantConnect(EnfantBase):
    password : str

class EnfantCreate(EnfantBase):
    nom : str
    prenom : str
    date_naissance : date
    classe:  str
    isWorking: bool
    work: int
    description: str
    nb_etoile :  int
    id_ortho :  int
    password : str

class Enfant(EnfantBase):
    nom : str
    prenom : str
    date_naissance : date
    classe:  str
    description : str
    isWorking: bool
    work: int
    nb_etoile :  int
    id_ortho :  int
    password : str
    id : int

    class Config:
        orm_mode = True

class EnfantGameBase(BaseModel):
    gender: int
    ethnicity: int
    instructions: str
    equipedItems: str
    horiz: bool
    locked_goose_easy: bool
    locked_goose_medium: bool
    locked_goose_hard: bool
    locked_listen_choose_easy: bool
    locked_listen_choose_medium: bool
    locked_listen_choose_hard: bool
    locked_memories_easy: bool
    locked_memories_medium: bool
    locked_memories_hard: bool
    id_enfant: int

class EnfantGlobal(Enfant):
    gender: int
    ethnicity: int
    instructions: str
    equipedItems: str
    horiz: bool
    locked_goose_easy: bool
    locked_goose_medium: bool
    locked_goose_hard: bool
    locked_listen_choose_easy: bool
    locked_listen_choose_medium: bool
    locked_listen_choose_hard: bool
    locked_memories_easy: bool
    locked_memories_medium: bool
    locked_memories_hard: bool

class EnfantGame(EnfantGameBase):
    id: int

    class Config:
        orm_mode = True

class Filtre(BaseModel):
    nom : str
    wordList: str
    id : int
    collaborative: bool
    collaborativeName: str
    id_ortho: int

    class Config:
        orm_mode = True

class FiltreCreate(BaseModel):
    nom: str
    wordList: str
    collaborative: bool
    collaborativeName: str
    id_ortho: int
    enfant_id: int

class FiltreUpdate(BaseModel):
    id: int
    wordList: str
    collaborative: bool
    collaborativeName: str

class FiltreEnfant(BaseModel):
    enfant_id: int
    id_filtre: int

class FiltreEnfantRemove(BaseModel):
    enfant_id: int

class OrthophonisteBase(BaseModel):
    nom : str
    prenom : str
    email : str
    grade : int

class OrthophonisteConnection(BaseModel):
    login: str
    password: str

class OrthophonisteCreate(OrthophonisteBase):
    password: str


class Orthophoniste(OrthophonisteBase):
    id: int
    enfants : List[Enfant] = []
    class Config:
        orm_mode = True

class GlobalInformationCreate(BaseModel):
    motVersion: int


class MotBase(BaseModel):
    orthographe : str
    nb_syll : int
    struct_syll: str
    struct_syll_type : str
    poly_syll: bool
    phonem : str
    id_ortho : int

class Mot(MotBase):
    id: int
    
    #liste_filtres : List[Liste_filtre] = []

    class Config:
        orm_mode = True

class CheckMot(BaseModel):
    bdd_version: int

class GetMot(CheckMot):
    mots: List[Mot]

class Liste_filtreBase(BaseModel):
    nom : str
    nb_syll : int
    struct_syll : list
    struct_syll_type : list
    phonem : list

class Liste_filtreCreate(Liste_filtreBase):
    pass

class Liste_mot(BaseModel):
    id:int
    mots: List[Mot] =[]

    class Config:
        orm_mode=True


class Liste_filtre(Liste_filtreBase):
    id: int
    
    mots : List[Mot] = []
    enfants : List[Enfant] = []

    class Config:
        orm_mode = True
   
class ResultatsCreate(BaseModel):
    login : str
    list_id : int
    date : datetime
    game : str
    difficulty : str
    success_percentage : float
    mots_rates : str
    timer: int


class Resultats(BaseModel):
    id_enfant : int
    id_filtre : int
    date : datetime
    nom_jeu : str
    difficulte : str
    taux_reussite : float
    mots_rates : str
    timer: int
    class Config:
        orm_mode = True

class Liste_EnfantBase(BaseModel):
    niveau : int
    date : datetime

class Liste_EnfantCreate(Liste_EnfantBase):
    pass

class Liste_Enfant(Liste_EnfantBase):

    class Config:
        orm_mode = True