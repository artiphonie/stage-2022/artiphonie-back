from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Float, DateTime, JSON
from sqlalchemy.orm import relationship, backref

from .database import Base

class Resultats(Base):
    __tablename__ = "resultats"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    id_enfant = Column(Integer, ForeignKey("enfant.id"), index=True)
    id_filtre = Column(Integer, index=True)
    date = Column(DateTime, index=True)
    nom_jeu = Column(String, index=True)
    difficulte =  Column(String, index=True)
    taux_reussite =  Column(Float, index=True)
    mots_rates =  Column(JSON, index=True)
    timer = Column(Integer, index=True)

class GlobalInformation(Base):
    __tablename__ = "globalinformation"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    motVersion = Column(Integer, index=True)

class Enfant(Base):
    __tablename__ = "enfant"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    nom = Column(String, index=True)
    prenom = Column(String, index=True)
    date_naissance = Column(DateTime, index=True)
    description = Column(String, index=True)
    classe =  Column(String, index=True)
    nb_etoile =  Column(Integer, index=True)
    isWorking = Column(Boolean, index=True) #Savoir si l'enfant à un travail qu'il doit réaliser
    work = Column(Integer, index=True) #Le travail qu'il doit réaliser (id du filtre appliqué)
    login =  Column(String, unique=True, index=True)
    password =  Column(String, index=True)
    id_ortho = Column(Integer,ForeignKey("orthophoniste.id"))

    orthophoniste = relationship("Orthophoniste", back_populates="enfants")

class EnfantGame(Base):
    __tablename__ = "enfantgame"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    gender = Column(Integer, index=True)
    ethnicity = Column(Integer, index=True)
    instructions = Column(String, index=True)
    equipedItems = Column(String, index=True)
    horiz = Column(Boolean, index=True)
    locked_goose_easy = Column(Boolean, index=True)
    locked_goose_medium = Column(Boolean, index=True)
    locked_goose_hard = Column(Boolean, index=True)
    locked_listen_choose_easy = Column(Boolean, index=True)
    locked_listen_choose_medium = Column(Boolean, index=True)
    locked_listen_choose_hard = Column(Boolean, index=True)
    locked_memories_easy = Column(Boolean, index=True)
    locked_memories_medium = Column(Boolean, index=True)
    locked_memories_hard = Column(Boolean, index=True)
    id_enfant = Column(Integer, index=True)

class Orthophoniste(Base):
    __tablename__ = "orthophoniste"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    nom = Column(String, index=True)
    prenom = Column(String, index=True)
    email = Column(String, unique=True, index=True)
    password =  Column(String, index=True)
    grade = Column(Integer, index=True)
    enfants = relationship("Enfant", back_populates="orthophoniste")


class Mot(Base):
    __tablename__ = "mot"
    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    orthographe = Column(String, index=True)
    nb_syll = Column(Integer, index=True)
    struct_syll = Column(String, index=True)
    struct_syll_type = Column(String, index=True)
    poly_syll = Column(Boolean, index=True)
    phonem = Column(String, index=True)
    id_ortho = Column(Integer, index=True)

class Filtre(Base):
    __tablename__ = "filtre"
    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    nom = Column(String, index=True)
    wordList = Column(String, index=True)
    collaborative = Column(Boolean, index=True)
    collaborativeName = Column(String, index=True)
    id_ortho = Column(Integer,ForeignKey("orthophoniste.id"), index=True)
