from operator import mod
from pyexpat import model
from tokenize import String
from unittest import result
from sqlalchemy import update
from sqlalchemy.orm import Session
import bcrypt
from . import models, schemas



''' -------- ENFANT -------- '''

def get_enfant(db: Session, enfant_id: int):
    return db.query(models.Enfant).filter(models.Enfant.id == enfant_id).all()

def get_enfant_only(db: Session, enfant_id: int):
    return db.query(models.Enfant).filter(models.Enfant.id == enfant_id).first()

def get_enfant_game(db: Session, enfant_id: int):
    return db.query(models.EnfantGame).filter(models.EnfantGame.id_enfant == enfant_id).all()

def get_enfant_ortho(db: Session, enfant_id: int, id_ortho):
    return db.query(models.Enfant).filter(models.Enfant.id == enfant_id).filter(models.Enfant.id_ortho == id_ortho).all()

def get_enfant_by_login(db: Session, enfant_login: str):
    return db.query(models.Enfant).filter(models.Enfant.login == enfant_login).first()

def get_enfants(db: Session, id_ortho: int = 0, skip: int = 0, limit: int = 100):
    return db.query(models.Enfant).filter(models.Enfant.id_ortho == id_ortho).offset(skip).limit(limit).all()

def connect_enfant(db: Session, enfant_login: str, enfant_password: str):
    enfant = db.query(models.Enfant).filter(models.Enfant.login == enfant_login).filter(models.Enfant.password == enfant_password).first()
    if enfant is None:
        return None
    enfantGame = db.query(models.EnfantGame).filter(models.EnfantGame.id_enfant == enfant.id).first()
    if enfantGame is None:
        return None
    enfant.gender = enfantGame.gender
    enfant.ethnicity = enfantGame.ethnicity
    enfant.instructions = enfantGame.instructions
    enfant.equipedItems = enfantGame.equipedItems
    enfant.horiz = enfantGame.horiz
    enfant.locked_goose_easy = enfantGame.locked_goose_easy
    enfant.locked_goose_medium = enfantGame.locked_goose_medium
    enfant.locked_goose_hard = enfantGame.locked_goose_hard
    enfant.locked_listen_choose_easy = enfantGame.locked_listen_choose_easy
    enfant.locked_listen_choose_medium = enfantGame.locked_listen_choose_medium
    enfant.locked_listen_choose_hard = enfantGame.locked_listen_choose_hard
    enfant.locked_memories_easy = enfantGame.locked_memories_easy
    enfant.locked_memories_medium = enfantGame.locked_memories_medium
    enfant.locked_memories_hard = enfantGame.locked_memories_hard
    return enfant

def create_default_enfant_game(db: Session, enfant_game: schemas.EnfantGameBase):
    db_enfant_game = models.EnfantGame(**enfant_game.dict())
    db.add(db_enfant_game)
    db.commit()
    db.refresh(db_enfant_game)
    return db_enfant_game

def create_enfant(db: Session, enfant : schemas.EnfantCreate):
    db_enfant =models.Enfant(**enfant.dict())
    db.add(db_enfant)
    db.commit()
    db.refresh(db_enfant)
    return db_enfant

def update_enfant(db: Session, enfant: schemas.Enfant):
    enfant_to_update = db.query(models.Enfant).filter(models.Enfant.id == enfant.id).first()
    #requete qui permet de voir si un autre utilisateur utilise deja le login voulu
    test_login_enfant = db.query(models.Enfant).filter(models.Enfant.login == enfant.login).filter(models.Enfant.id != enfant.id).all()
    if test_login_enfant:
        return None
    if enfant_to_update:
        enfant_to_update.login = enfant.login
        enfant_to_update.nom = enfant.nom
        enfant_to_update.prenom = enfant.prenom
        enfant_to_update.date_naissance = enfant.date_naissance
        enfant_to_update.description = enfant.description
        enfant_to_update.classe = enfant.classe
        enfant_to_update.nb_etoile = enfant.nb_etoile
        enfant_to_update.id_ortho = enfant.id_ortho
        enfant_to_update.password = enfant.password
        db.add(enfant_to_update)
        db.commit()
        db.refresh(enfant_to_update)
    return enfant_to_update

def update_enfant_game(db: Session, enfant: schemas.EnfantGameBase):
    enfant_game_update = db.query(models.EnfantGame).filter(models.EnfantGame.id_enfant == enfant.id_enfant).one()
    if enfant_game_update:
        enfant_game_update.gender = enfant.gender
        enfant_game_update.ethnicity = enfant.ethnicity
        enfant_game_update.equipedItems = enfant.equipedItems
        enfant_game_update.horiz = enfant.horiz
        enfant_game_update.instructions = enfant.instructions
        enfant_game_update.locked_goose_easy = enfant.locked_goose_easy
        enfant_game_update.locked_goose_medium = enfant.locked_goose_medium
        enfant_game_update.locked_goose_hard = enfant.locked_goose_hard
        enfant_game_update.locked_listen_choose_easy = enfant.locked_listen_choose_easy
        enfant_game_update.locked_listen_choose_medium = enfant.locked_listen_choose_medium
        enfant_game_update.locked_listen_choose_hard = enfant.locked_listen_choose_hard
        enfant_game_update.locked_memories_easy = enfant.locked_memories_easy
        enfant_game_update.locked_memories_medium = enfant.locked_memories_medium
        enfant_game_update.locked_memories_hard = enfant.locked_memories_hard

        db.add(enfant_game_update)
        db.commit()
        db.refresh(enfant_game_update)


''' --------- ORTHOPHONISTE --------- '''

def get_ortho(db: Session, id_ortho: int):
    return db.query(models.Orthophoniste).filter(models.Orthophoniste.id == id_ortho).all()

def create_orthophoniste(db: Session, orthophoniste : schemas.OrthophonisteCreate):
    db_orthophoniste= models.Orthophoniste(**orthophoniste.dict())
    db.add(db_orthophoniste)
    db.commit()
    db.refresh(db_orthophoniste)
    return db_orthophoniste

def connect_orthophoniste(db: Session, orthophoniste_login : str, orthophoniste_password : str):
    db_check = db.query(models.Orthophoniste).filter(models.Orthophoniste.email == orthophoniste_login).first()
    if db_check is None:
        return None
    password = orthophoniste_password.encode("utf-8")
    if bcrypt.checkpw(password, db_check.password):
        return db_check
    else:
        return None

def get_orthophonistes(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Orthophoniste).offset(skip).limit(limit).all()

def get_orthophoniste_by_id(db: Session, orthophoniste_id: int):
    return db.query(models.Orthophoniste).filter(models.Orthophoniste.id == orthophoniste_id).one()

def get_orthophoniste_enfant(db: Session, orthophoniste_login:str):
    orthophonistes = db.query(models.Orthophoniste).filter(models.Orthophoniste.login == orthophoniste_login).one()
    return db.query(models.Enfant).filter(models.Enfant.id_ortho == orthophonistes.id).all()

def update_ortho(db: Session, orthophoniste: schemas.Orthophoniste):
    ortho_to_update = db.query(models.Orthophoniste).filter(models.Orthophoniste.id == orthophoniste.id).one()
    if ortho_to_update:
        ortho_to_update.nom = orthophoniste.nom
        ortho_to_update.prenom = orthophoniste.prenom
        ortho_to_update.email = orthophoniste.email
        db.add(ortho_to_update)
        db.commit()
        db.refresh(ortho_to_update)
    return ortho_to_update

def update_ortho_password(db: Session, orthophoniste: schemas.OrthophonisteConnection):
    ortho_to_update = db.query(models.Orthophoniste).filter(models.Orthophoniste.email == orthophoniste.login).one()
    if ortho_to_update:
        orthoPwd = orthophoniste.password.encode("utf-8")
        salt = bcrypt.gensalt()
        hash = bcrypt.hashpw(orthoPwd, salt)
        ortho_to_update.password = hash
        db.add(ortho_to_update)
        db.commit()
        db.refresh(ortho_to_update)
    return ortho_to_update

def update_grade_ortho(db: Session, id_ortho: int, promote: int):
    ortho_to_update = db.query(models.Orthophoniste).filter(models.Orthophoniste.id == id_ortho).one()
    if ortho_to_update:
        ortho_to_update.grade = promote
        db.add(ortho_to_update)
        db.commit()
        db.refresh(ortho_to_update)
    return ortho_to_update


''' ------- MOTS -------- '''

def get_mot(db: Session, mot_id: int):
    return db.query(models.Mot).filter(models.Mot.id == mot_id).all()

def get_mots(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Mot).offset(skip).limit(limit).all()

def create_mot(db: Session, mot: schemas.MotBase):
    db_mot = models.Mot(**mot.dict())
    db.add(db_mot)
    db.commit()
    db.refresh(db_mot)
    return db_mot

def update_mot(db: Session, mot: schemas.Mot):
    mot_to_update = db.query(models.Mot).filter(models.Mot.id == mot.id).first()
    if mot_to_update:
        mot_to_update.orthographe = mot.orthographe
        mot_to_update.phonem = mot.phonem
        mot_to_update.poly_syll = mot.poly_syll
        mot_to_update.nb_syll = mot.nb_syll
        db.add(mot_to_update)
        db.commit()
        db.refresh(mot_to_update)
        return mot_to_update


def update_bdd_version(db: Session):
    bdd_version = db.query(models.GlobalInformation).first()
    bdd_version.motVersion = int(bdd_version.motVersion) + 1
    db.add(bdd_version)
    db.commit()
    db.refresh(bdd_version)

def get_bdd_version(db: Session):
    return db.query(models.GlobalInformation).first()


''' -------- FILTRES ---------- '''

def get_filtre(db: Session, filtre_id: int, id_ortho: int):
    return db.query(models.Filtre).filter(models.Filtre.id == filtre_id).filter(models.Filtre.id_ortho == id_ortho).first()

def get_filtre_collab_id(db: Session, filtre_id: int):
    return db.query(models.Filtre).filter(models.Filtre.id == filtre_id).first()

def get_filtre_all(db: Session, id_ortho: int):
    return db.query(models.Filtre).filter(models.Filtre.id_ortho == id_ortho).all()

def get_filtre_collab(db: Session, id_ortho: int):
    return db.query(models.Filtre).filter(models.Filtre.id_ortho != id_ortho).filter(models.Filtre.collaborative == True).all()

def update_filter(db: Session, filter_update: schemas.FiltreUpdate):
    filter_to_update = db.query(models.Filtre).filter(models.Filtre.id == filter_update.id).first()
    if filter_to_update:
        filter_to_update.wordList = filter_update.wordList
        filter_to_update.collaborative = filter_update.collaborative
        filter_to_update.collaborativeName = filter_update.collaborativeName
        db.add(filter_to_update)
        db.commit()
        db.refresh(filter_to_update)
        return filter_to_update

def select_work(db: Session, filtre: schemas.FiltreEnfant):
    enfant_update = db.query(models.Enfant).filter(models.Enfant.id == filtre.enfant_id).first()
    if enfant_update:
        enfant_update.isWorking = True
        enfant_update.work = filtre.id_filtre
        db.add(enfant_update)
        db.commit()
        db.refresh(enfant_update)
        return enfant_update

def remove_work(db: Session, filtre: schemas.FiltreEnfantRemove):
    enfant_update = db.query(models.Enfant).filter(models.Enfant.id == filtre.enfant_id).first()
    if enfant_update:
        enfant_update.isWorking = False
        db.add(enfant_update)
        db.commit()
        db.refresh(enfant_update)
        return enfant_update

def create_filtre(db: Session, filtre_create: schemas.FiltreCreate):
    db_filtre = models.Filtre()
    db_filtre.id_ortho = filtre_create.id_ortho
    db_filtre.nom = filtre_create.nom
    db_filtre.wordList = filtre_create.wordList
    db_filtre.collaborative = filtre_create.collaborative
    db_filtre.collaborativeName = filtre_create.collaborativeName
    db.add(db_filtre)
    db.commit()
    db.refresh(db_filtre)

    db_enfant_update = get_enfant_only(db, filtre_create.enfant_id)
    if not db_enfant_update:
        return None
    db_enfant_update.isWorking = True
    db_enfant_update.work = db_filtre.id
    db.add(db_enfant_update)
    db.commit()
    db.refresh(db_enfant_update)
    return db_filtre.id

"""def authentificate(login, password):
    utf_password = password.encode('utf-8')
    hashedPassword = bcrypt.hashpw(utf_password, bcrypt.gensalt())
    print("==================THE HASSHED PASSWORD====================")
    print(hashedPassword)
    if bcrypt.checkpw(utf_password, hashedPassword):
        print("login success")
        return True
    else:
        print("incorrect password")
        return False

"""

def get_listes_filtre(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Liste_filtre).offset(skip).limit(limit).all()

def get_liste_by_login(db: Session, login: str):
    enfant = get_enfant_by_login(db,login)
    #table_enfant = db.query(models.liste_enfant).filter(models.liste_enfant.c.id_enfant==enfant.id).filter(func.max(models.liste_enfant.c.date)).first()
    liste_mot= schemas.Liste_mot
    liste_mot.id=enfant.id
    liste_mot.mots=get_mots(db,0,100)
    return liste_mot

def create_resultats(db: Session, resultats):
    enfant = get_enfant_by_login(db,resultats.login)
    resultats_toAdd = models.Resultats()
    resultats_toAdd.id_enfant =  enfant.id
    resultats_toAdd.id_filtre = resultats.list_id
    resultats_toAdd.date = resultats.date
    resultats_toAdd.nom_jeu = resultats.game
    resultats_toAdd.difficulte = resultats.difficulty
    resultats_toAdd.taux_reussite = resultats.success_percentage
    resultats_toAdd.mots_rates = resultats.mots_rates
    resultats_toAdd.timer = resultats.timer
    db.add(resultats_toAdd)
    db.commit()
    db.refresh(resultats_toAdd)
    return db.query(models.Resultats).filter(models.Resultats.id_enfant==resultats_toAdd.id_enfant).filter(models.Resultats.id_filtre==resultats_toAdd.id_filtre).filter(models.Resultats.date == resultats_toAdd.date).first()

def get_resultats(db: Session, id_enfant: int, skip: int = 0, limit: int = 0):
    return db.query(models.Resultats).filter(models.Resultats.id_enfant == id_enfant).offset(skip).limit(limit).all()