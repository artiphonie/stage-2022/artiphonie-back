from genericpath import exists
from time import sleep
from tokenize import String
from typing import List
from urllib import response

from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy import inspect

from sqlalchemy.orm import Session
from fastapi.middleware.cors import CORSMiddleware

from . import crud, models, schemas
from .database import SessionLocal, engine

from datetime import datetime

import bcrypt
import json

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:8080"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

inspector = inspect(engine)

showTable = True
if showTable:
    for table_name in inspector.get_table_names():
        print("TABLE===", table_name)
        for column in inspector.get_columns(table_name):
            print("Column: %s" % column['name'])

def init_bdd(db: Session = Depends(get_db)):
    db_check = db.query(models.GlobalInformation).first()
    if db_check is None:
        print("Init BDD")

        ''' ----- Init Global ----- '''
        if exists("globalInformation.json"):
            print("Update Global")
            with open('globalInformation.json', encoding='utf-8-sig') as f:
                d = json.load(f)
                for row in d:
                    db_global = models.GlobalInformation(motVersion=row["motVersion"])
                    db.add(db_global)
                    db.commit()
                    db.refresh(db_global)
        else:
            db_global = models.GlobalInformation(motVersion=1)
            db.add(db_global)
            db.commit()
            db.refresh(db_global)

        ''' ----- Init Admin Account ----- '''

        if exists("orthophonistes.json"):
            print("Update orthophonistes")
            with open('orthophonistes.json', encoding='utf-8-sig') as f:
                d = json.load(f)
                for row in d:
                    db_global = models.Orthophoniste(nom=row["nom"], prenom=row["prenom"], email=row["email"], password=row["password"].encode("utf-8"), grade=row["grade"])
                    db.add(db_global)
                    db.commit()
                    db.refresh(db_global)
        else:
            orthoBf = "admin"
            orthoPwd = orthoBf.encode("utf-8")
            salt = bcrypt.gensalt()
            hash = bcrypt.hashpw(orthoPwd, salt)
            db_orthophoniste= models.Orthophoniste(nom="admin", prenom="admin", email="admin@admin.fr", password=hash, grade=5)
            db.add(db_orthophoniste)
            db.commit()
            db.refresh(db_orthophoniste)

        ''' ----- Init Enfants ----- '''
        if exists("enfants.json"):
            print("Update Enfants")
            with open('enfants.json', encoding='utf-8-sig') as f:
                d = json.load(f)
                for row in d:
                    db_global = models.Enfant(nom=row["nom"], prenom=row["prenom"], date_naissance=datetime.fromisoformat(row["date_naissance"]), classe=row["classe"], isWorking=row["isWorking"], login=row["login"], id_ortho=row["id_ortho"], description=row["description"], nb_etoile=row["nb_etoile"], work=row["work"], password=row["password"])
                    db.add(db_global)
                    db.commit()
                    db.refresh(db_global)

        if exists("enfantsgame.json"):
            print("Update Enfants Game")
            with open('enfantsgame.json', encoding='utf-8-sig') as f:
                d = json.load(f)
                for row in d:
                    db_global = models.EnfantGame(ethnicity=row["ethnicity"], equipedItems="", horiz=row["horiz"], instructions=row["instructions"], locked_goose_medium=row["locked_goose_medium"], gender=row["gender"], locked_goose_easy=row["locked_goose_easy"], locked_goose_hard=row["locked_goose_hard"], locked_listen_choose_easy=row["locked_listen_choose_easy"], locked_listen_choose_medium=row["locked_listen_choose_medium"], locked_listen_choose_hard=row["locked_listen_choose_hard"], locked_memories_easy=row["locked_memories_easy"], locked_memories_medium=row["locked_memories_medium"], locked_memories_hard=row["locked_memories_hard"], id_enfant=row["id_enfant"])
                    db.add(db_global)
                    db.commit()
                    db.refresh(db_global)

        ''' ----- Init Filtre ----- '''
        if exists("filtres.json"):
            print("Update filtres")
            with open('filtres.json', encoding='utf-8-sig') as f:
                d = json.load(f)
                for row in d:
                    db_global = models.Filtre(nom=row["nom"], wordList=row["wordList"], id_ortho=row["id_ortho"], collaborative=False, collaborativeName="")
                    db.add(db_global)
                    db.commit()
                    db.refresh(db_global)

        ''' ----- Init Resultats ----- '''
        if exists("resultats.json"):
            print("Update resultats")
            with open('resultats.json', encoding='utf-8-sig') as f:
                d = json.load(f)
                for row in d:
                    db_global = models.Resultats(date=datetime.fromisoformat(row["date"]), difficulte=row["difficulte"], mots_rates=row["mots_rates"], id_enfant=row["id_enfant"], nom_jeu=row["nom_jeu"], taux_reussite=row["taux_reussite"])
                    db.add(db_global)
                    db.commit()
                    db.refresh(db_global)

        ''' ----- Init Words ----- '''
        print("Start init words - please wait until the end")
        with open('words.json', encoding='utf-8-sig') as f:
            d = json.load(f)
            print("Total to add: {}".format(len(d)))
            cmpt = 0
            for item in d:
                cmpt += 1
                b = "Word n: " + str(cmpt)
                print(b, end="\r")
                db_mot = models.Mot(orthographe=item["orthographe"], nb_syll=item["nb_syll"], poly_syll=item["poly_syll"], struct_syll=item["struct_syll"], struct_syll_type=item["struct_syll_type"], phonem=item["phonem"] , id_ortho=0)
                db.add(db_mot)
                db.commit()
                db.refresh(db_mot)
        print("End init bdd")

@app.on_event("startup")
def start():
    print("Start programm")

@app.get("/")
def home(db: Session = Depends(get_db)):
    init_bdd(db)
    return "Hello from FastAPI v2 !!!!"

''' --------- ENFANTS --------- '''

@app.get("/enfants", response_model=List[schemas.Enfant])
def read_enfants(id_ortho: int = 0, skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    enfants = crud.get_enfants(db, id_ortho=id_ortho, skip=skip, limit=limit)
    return enfants


@app.get("/enfants/search", response_model=List[schemas.Enfant])
def read_enfant(enfant_id: int, id_ortho: int, db: Session = Depends(get_db)):
    db_enfant = crud.get_enfant_ortho(db, enfant_id=enfant_id, id_ortho=id_ortho)
    if not db_enfant:
        raise HTTPException(status_code=404, detail="Enfant not found")
    return db_enfant

@app.get("/enfants/work", response_model=schemas.Filtre)
def enfant_work(enfant_id: int, db: Session = Depends(get_db)):
    db_enfant = db.query(models.Enfant).filter(models.Enfant.id == enfant_id).first()
    if db_enfant is None:
        raise HTTPException(status_code=404, detail="Enfant not found")
    if db_enfant.isWorking:
        db_work = db.query(models.Filtre).filter(models.Filtre.id == db_enfant.work).first()
        if db_work is None:
            enfant_update = db.query(models.Enfant).filter(models.Enfant.id == enfant_id).first()
            if enfant_update:
                enfant_update.isWorking = False
                db.add(enfant_update)
                db.commit()
                db.refresh(enfant_update)
            raise HTTPException(status_code=404, detail="Filtre not found")
        return db_work
    else:
        raise HTTPException(status_code=404, detail="No work to do")
    

@app.post("/enfants/connection",response_model=schemas.EnfantGlobal)
def connect_enfant(enfant: schemas.EnfantConnect , db :Session = Depends(get_db)):
    enfant.login = enfant.login.lower()
    enfant = crud.connect_enfant(db, enfant_login=enfant.login, enfant_password=enfant.password)
    if enfant is None:
        raise HTTPException(status_code=404, detail="Connection failed")
    print(enfant)
    return enfant

@app.post("/enfants/update",response_model=schemas.Enfant)
def update_enfant(enfant : schemas.Enfant, db= Depends(get_db)):
    enfant =  crud.update_enfant(db, enfant=enfant)
    if enfant is None:
        raise HTTPException(status_code=404, detail="Enfant not found")
    return enfant

@app.post("/enfants/update/game")
def update_enfant_game(enfant: schemas.EnfantGameBase, db = Depends(get_db)):
    crud.update_enfant_game(db=db, enfant=enfant)

@app.put("/enfants/create/game")
def create_default_enfant_game(enfant_game: schemas.EnfantGameBase, db: Session = Depends(get_db)):
    crud.create_default_enfant_game(db=db, enfant_game=enfant_game)

@app.put("/enfants/create", response_model=schemas.Enfant)
def create_enfant(enfant: schemas.EnfantCreate, db: Session = Depends(get_db)):
    enfant.login = enfant.login.lower()
    db_check = db.query(models.Enfant).filter(models.Enfant.login == enfant.login).first()
    if db_check is None:
        return crud.create_enfant(db=db, enfant=enfant)
    else:
        raise HTTPException(status_code=404, detail="Login already used")

@app.delete("/enfants/remove")
def remove_enfant(enfant_id: int, db: Session = Depends(get_db)):
    db_enfant = crud.get_enfant(db, enfant_id=enfant_id)
    if not db_enfant:
        raise HTTPException(status_code=404, detail="Enfant not found")
    db.delete(db_enfant[0])
    db.commit()
    db_enfant_game = crud.get_enfant_game(db, enfant_id=enfant_id)
    if db_enfant_game:
        db.delete(db_enfant_game[0])
        db.commit()
    db_result = crud.get_resultats(db, enfant_id, 0, 100000)
    if db_result:
        for result in db_result:
            db.delete(result)
            db.commit()
    


''' --------- ORTHOPHONISTES --------- '''

@app.get("/orthophonistes", response_model=List[schemas.Orthophoniste])
def read_orthophoniste(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    orthophonistes = crud.get_orthophonistes(db, skip=skip, limit=limit)
    return orthophonistes

@app.get("/orthophonistes/get_enfants", response_model=List[schemas.Enfant])
def orthophonistes_enfants(orthophoniste_login: str, db: Session = Depends(get_db)):
    enfants = crud.get_orthophoniste_enfant(db, orthophoniste_login)
    if enfants is None:
        raise HTTPException(status_code=404, detail="Enfant not found")
    return enfants

@app.post("/orthophonistes/connection", response_model=schemas.Orthophoniste)
def connect_orthophoniste(orthophoniste: schemas.OrthophonisteConnection, db :Session = Depends(get_db)):
    orthophoniste = crud.connect_orthophoniste(db, orthophoniste_login=orthophoniste.login, orthophoniste_password=orthophoniste.password)
    if orthophoniste is None:
       raise HTTPException(status_code=404, detail="Connection failed")
    return orthophoniste

@app.post("/orthophonistes/update")
def update_ortho(orthophoniste: schemas.Orthophoniste, db: Session = Depends(get_db)):
    orthophoniste =  crud.update_ortho(db, orthophoniste=orthophoniste)
    if orthophoniste is None:
        raise HTTPException(status_code=404, detail="Orthophoniste not found")

@app.post("/orthophonistes/update/password")
def update_ortho(orthophoniste: schemas.OrthophonisteConnection, db: Session = Depends(get_db)):
    orthophoniste =  crud.update_ortho_password(db, orthophoniste=orthophoniste)
    if orthophoniste is None:
        raise HTTPException(status_code=404, detail="Orthophoniste not found")

@app.post("/orthophonistes/promote")
def promote_ortho(idOrtho: int, promote: int, db: Session = Depends(get_db)):
    db_ortho = crud.update_grade_ortho(db, id_ortho=idOrtho, promote=promote)
    if not db_ortho:
        raise HTTPException(status_code=404, detail="Ortho not found")

@app.put("/orthophonistes/create", response_model=schemas.Orthophoniste)
def create_orthophoniste(orthophoniste: schemas.OrthophonisteCreate, db: Session = Depends(get_db)):
    db_check = db.query(models.Orthophoniste).filter(models.Orthophoniste.email == orthophoniste.email).first()
    if db_check is None:
        orthoPwd = orthophoniste.password.encode("utf-8")
        salt = bcrypt.gensalt()
        hash = bcrypt.hashpw(orthoPwd, salt)
        orthophoniste.password = hash
        orthophoniste.grade = 0
        return crud.create_orthophoniste(db=db, orthophoniste=orthophoniste)
    else:
        raise HTTPException(status_code=404, detail="Email already used") 

@app.delete("/orthophonistes/remove")
def remove_ortho(id_ortho: int, db: Session = Depends(get_db)):
    db_ortho = crud.get_ortho(db, id_ortho=id_ortho)
    if not db_ortho:
        raise HTTPException(status_code=404, detail="Ortho not found")
    db.delete(db_ortho[0])
    db.commit()

    db_enfants = crud.get_enfants(db, id_ortho, 0, 100000)
    for enfant in db_enfants:
        db_result = crud.get_resultats(db, id_enfant=enfant.id, skip=0,limit=100000)
        for result in db_result:
            db.delete(result)
            db.commit()
        db.delete(enfant)
        db.commit()
    
    db_filtres = crud.get_filtre_all(db, id_ortho)
    for filtre in db_filtres:
        db.delete(filtre)
        db.commit()

''' -------- MOTS -------- '''

@app.get("/mots", response_model=schemas.GetMot)
def read_mots(skip: int = 0, limit: int = 2000, db: Session = Depends(get_db)):
    mot = crud.get_mots(db, skip=skip, limit=limit)
    db_version = crud.get_bdd_version(db)
    return {"bdd_version": db_version.motVersion, "mots": mot}

@app.post("/mots/update")
def update_mots(mot : schemas.Mot, db= Depends(get_db)):
    mot_up = crud.update_mot(db, mot=mot)
    if mot_up is None:
        raise HTTPException(status_code=404, detail="Mot not found")
    crud.update_bdd_version(db)

@app.post("/mots/check")
def check_bdd_version(check: schemas.CheckMot ,db: Session = Depends(get_db)):
    db_version = crud.get_bdd_version(db)
    if db_version.motVersion != check.bdd_version:
        return {"valid": False, "complete": read_mots(skip=0, limit=5000, db=db)}
    return {"valid": True}


@app.put("/mots/create", response_model=schemas.Mot)
def create_mot(mot: schemas.MotBase, db: Session = Depends(get_db)):
    db_check = db.query(models.Mot).filter(models.Mot.orthographe == mot.orthographe.lower()).first()
    db_check_two = db.query(models.Mot).filter(models.Mot.orthographe == mot.orthographe).first()
    if db_check is None and db_check_two is None:
        crud.update_bdd_version(db)
        return crud.create_mot(db=db, mot=mot)
    else:
        raise HTTPException(status_code=404, detail="Mot already exist")

@app.delete("/mots/remove")
def remove_mot(mot_id: int, db: Session = Depends(get_db)):
    db_mot = crud.get_mot(db, mot_id=mot_id)
    if not db_mot:
        raise HTTPException(status_code=404, detail="Mot not found")
    db.delete(db_mot[0])
    db.commit()
    crud.update_bdd_version(db)

''' --------- FILTRES --------- '''

@app.get("/filtres")
def get_filtres(id_ortho: int, db: Session = Depends(get_db)):
    return crud.get_filtre_all(db, id_ortho)

@app.get("/filtres/{id_ortho}/{filtre_id}")
def get_filtre(id_ortho: int, filtre_id: int, db: Session = Depends(get_db)):
    db_filtre = crud.get_filtre(db, filtre_id, id_ortho)
    if not db_filtre:
        raise HTTPException(status_code=404, detail="Filtre not found")
    return db_filtre

@app.get("/filtres/{filtre_id}/c/collab")
def get_filtre(filtre_id: int, db: Session = Depends(get_db)):
    db_filtre = crud.get_filtre_collab_id(db, filtre_id)
    if not db_filtre:
        raise HTTPException(status_code=404, detail="Filtre not found")
    return db_filtre

@app.get("/filtres/collaborative")
def get_collaborative(id_ortho: int, db: Session = Depends(get_db)):
    return crud.get_filtre_collab(db, id_ortho)

@app.post("/filtres/update")
def update_filtre(filtre_update: schemas.FiltreUpdate, db: Session = Depends(get_db)):
    filter_up = crud.update_filter(db, filtre_update)
    if filter_up is None:
        raise HTTPException(status_code=404, detail="Filter not found")

@app.post("/filtres/work")
def select_work(filtre: schemas.FiltreEnfant, db: Session = Depends(get_db)):
    filtre_enfant = crud.select_work(db, filtre)
    if filtre_enfant is None:
        raise HTTPException(status_code=404, detail="Filter of Enfant not found")

@app.post("/filtres/work/remove")
def remove_work(filtre: schemas.FiltreEnfantRemove, db: Session = Depends(get_db)):
    filtre_enfant = crud.remove_work(db, filtre)
    if filtre_enfant is None:
        raise HTTPException(status_code=404, detail="Enfant not found")

@app.put("/filtres/create")
def create_filtre(filtre_create: schemas.FiltreCreate, db: Session = Depends(get_db)):
    db_filtre = crud.create_filtre(db, filtre_create)
    if not db_filtre:
        raise HTTPException(status_code=404, detail="Error while put data")
    return db_filtre

@app.delete("/filtres/remove")
def delete_filtre(filtre_id: int, id_ortho: int, db: Session = Depends(get_db)):
    db_filtre = crud.get_filtre(db, filtre_id, id_ortho)
    if not db_filtre:
        raise HTTPException(status_code=404, detail="Filtre not found")
    db.delete(db_filtre)
    db.commit()

''' --------- RESULTATS --------- '''

@app.get("/resultats")
def read_resultats(id_enfant: int,skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return crud.get_resultats(db, id_enfant, skip=skip,limit=limit)

@app.put("/resultats/create")
def create_resultats(resultats: schemas.ResultatsCreate, db: Session = Depends(get_db)):
    return crud.create_resultats(db=db, resultats=resultats)

@app.get("/star_increment")
def incrementStar(idPlayer: int, qte: int, db: Session = Depends(get_db)):
    db_enfant = crud.get_enfant(db, enfant_id=idPlayer)
    if not db_enfant:
        raise HTTPException(status_code=404, detail="Enfant not found")
    enfant_to_update = db.query(models.Enfant).filter(models.Enfant.id == idPlayer).first()
    if enfant_to_update:
        enfant_to_update.nb_etoile = db_enfant[0].nb_etoile + qte
        db.add(enfant_to_update)
        db.commit()
        db.refresh(enfant_to_update)


@app.get("/save/mots")
def get_all_words(db: Session = Depends(get_db)):
    return db.query(models.Mot).all()

@app.get("/save/enfants")
def save_enfants(db: Session = Depends(get_db)):
    return db.query(models.Enfant).all()

@app.get("/save/enfants/game")
def save_enfants_game(db: Session = Depends(get_db)):
    return db.query(models.EnfantGame).all()

@app.get("/save/filtres")
def save_filtre(db: Session = Depends(get_db)):
    return db.query(models.Filtre).all()

@app.get("/save/globalInformation")
def save_global(db: Session = Depends(get_db)):
    return db.query(models.GlobalInformation).all()

@app.get("/save/orthophonistes")
def save_orthophoniste(db: Session = Depends(get_db)):
    return db.query(models.Orthophoniste).all()

@app.get("/save/resultats")
def save_result(db: Session = Depends(get_db)):
    return db.query(models.Resultats).all()