ARTIPHONIE - Serveur Web
================================================================

*Vous trouverez dans ce répertoire tout ce qui constitue notre serveur web, développé en Python.   
L'API, développé avec FastAPI, fournira des requêtes à la fois à l'interface web à destination des orthophonistes, et à l'application Godot, à destination des enfants.*

Pour lancer le container Docker avec l'API, il faut faire la commande suivante dans un terminal :
```shell
docker-compose up -d --build
```
Vous pourrez ensuite accéder à l'API à l'adresse http://localhost:5000 , et à l'interface Swagger UI à l'adresse http://localhost:5000/docs

Pour consulter la liste des containers :
```shell
docker-compose ps
```

Pour arréter les processus actifs :
```shell
docker-compose stop
```
Ils pourront être redémarrer avec :
```shell
docker-compose start
```

La liste des commandes docker-compose ici : https://docs.docker.com/compose/reference/

